// To compile and link with VC++ 8, I did (all in one line):
// D:\Dev\Projects\PerlTools>cl /EHsc /I../Libs BalanceDetailMap.cpp
//    ..\Libs\build\win32\vc8\release\Bitmap\Bitmap.obj ..\..\ExtLibs\libpng-1.2.8\build\win32\vc8\release\cfs_png.lib
//    ..\..\ExtLibs\libjpeg-6b\build\win32\vc8\release\cfs_jpeg.lib ..\..\ExtLibs\zlib-1.2.2\build\win32\vc8\release\cfs_z.lib
#include <iostream>
#include "Bitmap/Bitmap.hpp"


int main(int ArgC, const char* ArgV[])
{
    if (ArgC!=3)
    {
        std::cout << "Usage: BalanceDetailMap DetailMap BlurredDetailMap\n";
        return 1;
    }

    BitmapT DetailMap;
    BitmapT BlurredDetailMap;

    try
    {
        std::cout << "Reading detail map " << ArgV[1] << "...\n";
        DetailMap=BitmapT(ArgV[1]);
    }
    catch (const BitmapT::LoadErrorT& E)
    {
        std::cout << "Could not open " << ArgV[1] << "\n";
        return 1;
    }

    try
    {
        std::cout << "Reading blurred detail map " << ArgV[2] << "...\n";
        BlurredDetailMap=BitmapT(ArgV[2]);
    }
    catch (const BitmapT::LoadErrorT& E)
    {
        std::cout << "Could not open " << ArgV[2] << "\n";
        return 1;
    }

    if (DetailMap.SizeX!=BlurredDetailMap.SizeX || DetailMap.SizeY!=BlurredDetailMap.SizeY)
    {
        std::cout << "The two input maps are not of the same size!\n";
        std::cout << ArgV[1] << " is " << DetailMap.SizeX << "*" << DetailMap.SizeY << ",\n";
        std::cout << ArgV[2] << " is " << BlurredDetailMap.SizeX << "*" << BlurredDetailMap.SizeY << ".\n";
        return 1;
    }


    BitmapT Out(DetailMap.SizeX, DetailMap.SizeY);

    for (unsigned long y=0; y<DetailMap.SizeY; y++)
        for (unsigned long x=0; x<DetailMap.SizeX; x++)
        {
            int r1, g1, b1, a1;
            DetailMap.GetPixel(x, y, r1, g1, b1, a1);

            int r2, g2, b2;
            BlurredDetailMap.GetPixel(x, y, r2, g2, b2);

            Out.SetPixel(x, y, r1-r2+128, g1-g2+128, b1-b2+128, a1);
        }

    if (!Out.SaveToDisk("FixedDetailMap.png"))
    {
        std::cout << "Could not save the results to FixedDetailMap.png!\n";
        return 1;
    }


    std::cout << "Results successfully saved to FixedDetailMap.png!\n";
    return 0;
}
