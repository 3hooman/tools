ImportError: No module named paramiko
=====================================

1. Under Windows, install a prebuilt binary of PyCrypto from
   http://www.voidspace.org.uk/python/modules.shtml#pycrypto

   As we use 32-bit versions of Python 2.7 (as explained at
   http://www.cafu.de/wiki/cppdev:gettingstarted#python_and_scons), usually
   the right choice is
   http://www.voidspace.org.uk/downloads/pycrypto26/pycrypto-2.6.win32-py2.7.exe

2. Both under Windows and Linux, install Paramiko at the command line with:

       pip install paramiko

   This should work out of the box. If pip for some reason is not installed on
   your system yet, install it as explained at
   http://www.pip-installer.org/en/latest/installing.html
