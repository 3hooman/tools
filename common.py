#!/usr/bin/env python
# -*- coding: utf-8 -*-
import colorama
import glob
import os
import shutil
import subprocess
import sys
from datetime import datetime


# Colorama makes ANSI escape character sequences also work under MS Windows.
# These sequences are used for producing colored terminal text and for cursor positioning.
# See http://pypi.python.org/pypi/colorama for details.
# Under Ubuntu, install the colorama module via "sudo easy_install colorama".
colorama.init()


RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[1;%dm"
BOLD_SEQ = "\033[1m"
BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(30, 38)


def Warn(x):
    return (COLOR_SEQ % (RED)) + str(x) + RESET_SEQ


def Info(x, col=GREEN):
    return (COLOR_SEQ % (col)) + str(x) + RESET_SEQ


def zeit():
    return datetime.now().strftime("%H:%M:%S,")


def GetSvnHeadRev(svn_trunk_url):
    svn_info = subprocess.Popen(["svn", "info", svn_trunk_url], stdout=subprocess.PIPE).communicate()[0]
    head_rev = None

    for line in svn_info.split("\n"):
        if line.lower().startswith("last changed rev:"):
            head_rev = line[17:].strip()
            break
        elif line.lower().startswith("letzte geänderte rev:"):
            head_rev = line[23:].strip()
            break

    if head_rev is None:
        raise Exception("Could not determine head_rev.")

    return head_rev


def FindSConsBuildDir(root="."):
    """
    Starting at the given Cafu root directory, this method guesses where all the executable program
    files that were built by a previously successful run of SCons are.
    """
    if sys.platform == "win32":
        for compiler in ["vc15", "vc14", "vc13", "vc12", "vc11", "vc10", "vc9", "vc8"]:
            for arch in ["amd64", "x64", "x86"]:
                path = "build/" + sys.platform + "/" + compiler + "/" + arch + "/release"
                if os.path.isfile(root + "/" + path + "/CaBSP/CaBSP.exe"):
                    return path
    else:
        for compiler in ["g++"]:
            path = "build/" + sys.platform + "/" + compiler + "/release"
            if os.path.isfile(root + "/" + path + "/CaBSP/CaBSP"):
                return path

    raise Exception("Could not find the SCons build directory.")


def CopyRepoToBinary(SourceDir, DestDir):
    """
    Creates a "binary release" directory `DestDir` from a Cafu source code repository `SourceDir`.

    The directory `SourceDir` must be the root directory of a Cafu source code repository checkout
    in which in which all programs and libraries have already successfully been built, all maps files
    have already been compiled, all texture images are present, etc.

    The directory `DestDir` is newly created and intended to be used as a public, binary release of the
    Cafu Engine and games developed with it (possibly after further packaging, e.g. as a zip file).
    An exeption is thrown if `DestDir` already exists.
    """

    def SkipCallback(path, names):
        """
        This function is called by the recursive directory copy below.
        It defines the items that should be skipped while everything else is being copied.
        """
        relpath = os.path.normpath(os.path.relpath(path, SourceDir))

        # print "path:", path, "relpath:", relpath, "names:", names, "\n"

        if relpath == os.path.normpath("Games/DeathMatch"):
            return ["Code"]

        if relpath == os.path.normpath("Games/DeathMatch/Music"):
            return ["NextTitle.txt"]

        if relpath == os.path.normpath("Games/DeathMatch/Terrains"):
            return ["TechDemo"]

        if relpath == os.path.normpath("Games/DeathMatch/Textures"):
            return ["D3Test", "Q1"]

        if relpath == os.path.normpath("Games/DeathMatch/Worlds"):
            # TODO: Ignore some of the less interesting worlds?
            pass

        # By default, ignore nothing (copy everything).
        return []

    def Copy(patterns, relative_dest_dir=None):
        """
        A custom method for conveniently copying individual or sets of files.
        """
        if isinstance(patterns, basestring):
            patterns = [patterns]

        for p in patterns:
            dd = DestDir + "/" + (relative_dest_dir or os.path.dirname(p))

            if not os.path.isdir(dd):
                os.makedirs(dd)

            files = glob.glob(SourceDir + "/" + p)

            if not files:
                print "Could not copy", p, "--", Warn("source file(s) not found")

            for f in files:
                if f.endswith("/CaWE"):
                    # Under Linux, copy CaWE to CaWE-bin, in order to not name-clash with the directory named "CaWE"!
                    shutil.copy2(f, dd + "/CaWE-bin")
                else:
                    # The normal case.
                    shutil.copy2(f, dd)

    # Copy the directories of which (almost) all files can be copied.
    LocalDirs = [
        "CaWE/res",
        "Fonts",
        "Games/DeathMatch",
        "Misc"
    ]

    for d in LocalDirs:
        shutil.copytree(SourceDir + "/" + d, DestDir + "/" + d, ignore=SkipCallback)

    # Copy some files that were ignored in the above step.
    Copy("Games/DeathMatch/Textures/Q1/sliptopsd*")
    Copy(["Games/DeathMatch/Terrains/TechDemo/" + f for f in [
        "Island.ter", "Island-BaseTex.jpg", "Island-Readme.txt", "Island-TerragenSettings.tgw", "Island-WorldMachineFile.tmd"]])

    # Setup common variables for the next steps.
    SCONS_BUILD_DIR = FindSConsBuildDir(SourceDir)
    ExePattern = ".exe*" if sys.platform == "win32" else ""     # Under Windows, copy .exe and .exe.manifest files.
    DllPattern = "*.dll" if sys.platform == "win32" else "lib*.so"

    # Copy all program executables.
    Programs = [
        "Ca3DE/Cafu",
        "CaBSP/CaBSP",
        "CaPVS/CaPVS",
        "CaLight/CaLight",
        "CaSHL/CaSHL",
        "CaWE/CaWE",        # Treated as a special case in Copy().
      # "CaSanity",         # Not for the demo...
        "MakeFont",
        "MaterialViewer",
      # "ModelViewer"       # This one is not even built any more.
    ]

    for p in Programs:
        Copy(SCONS_BUILD_DIR + "/" + p + ExePattern, ".")

    # Copy all renderer and sound DLLs.
    for d in ["MaterialSystem", "SoundSystem"]:
        Copy("Libs/" + SCONS_BUILD_DIR + "/" + d + "/" + DllPattern, ".")

    # Copy the relevant files in the top-level directory (last step).
    Copy(["config.lua", DllPattern, "*.txt"])
