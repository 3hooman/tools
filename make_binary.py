#!/usr/bin/env python
# -*- coding: utf-8 -*-
import paramiko     # If this line causes an error, consult README.txt in the same directory.
import argparse, glob, multiprocessing, os, platform, shutil, subprocess, sys, tempfile, traceback, zipfile
from common import Info, Warn, zeit, FindSConsBuildDir


parser = argparse.ArgumentParser(description='Automatically create a binary release of the Cafu Engine for the current platform, and upload it to the Cafu website.')
parser.add_argument('--username', action="store", help='the FTP username for uploading the files')
parser.add_argument('--password', action="store", help='the FTP password for uploading the files')
parser.add_argument('--tzip',     action="store", help='the path and name to the zip file with all the textures')
parser.add_argument('--wzip',     action="store", help='the path and name to the zip file with all the worlds')
parser.add_argument('--fbx',      action="store", help='the path to the FBX SDK installation')

# Under Linux, in order to shutdown the computer after successful completion of the script:
#     sudo sh -c './make_binary.py --username <USERNAME> --password <PASSWORD> && shutdown -h +15'
if sys.platform == "win32":
    parser.add_argument('--shutdown', action="store_true", help='shutdown the computer after successful completion of the script')

args         = parser.parse_args()
git_repo_url = "git@bitbucket.org:cafu/cafu.git"
ftp_username = args.username or raw_input("FTP username: ")
ftp_password = args.password or raw_input("FTP password: ")
Textures_zip = os.path.abspath(args.tzip or os.path.expanduser("~/Downloads/Textures.zip"))
Worlds_zip   = os.path.abspath(args.wzip or os.path.expanduser("~/Downloads/Worlds.zip"))
fbx_abspath  = args.fbx and os.path.abspath(args.fbx)  # Assign something only if args.fbx has a value.


def myMove(patterns, nach):
    if isinstance(patterns, basestring):
        patterns = [patterns]

    for p in patterns:
        files = glob.glob(p)

        if not files:
            print "move", p, "→", nach, Warn("source file(s) not found")

        for von in files:
            print "move", von, "→", nach,
            try:
                shutil.move(von, nach)
                print Info("ok")
            except shutil.Error as e:
                print Warn("{0}".format(e))


def Main():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # Test the FTP login credentials.
    print "\n" + zeit(), "Test the FTP login credentials."
    ssh.connect('ftp.cafu.de', username=ftp_username, password=ftp_password)
    ssh.close()
    print Info("valid")


    # Preliminary checks: Bail out early if there are errors.
    if not zipfile.is_zipfile(Textures_zip):
        raise Exception('"{0}" does not seem to be the Textures.zip file.'.format(Textures_zip))

    if not zipfile.is_zipfile(Worlds_zip):
        raise Exception('"{0}" does not seem to be the Worlds.zip file.'.format(Worlds_zip))

    if not fbx_abspath:
        print Warn("No FBX SDK location given -- building without FBX support!")
    else:
        if not (os.path.isdir(fbx_abspath + "/include") and os.path.isdir(fbx_abspath + "/lib")):
            raise Exception('FBX SDK not found at "{0}".'.format(fbx_abspath))


    # Create the working directory.
    print "\n" + zeit(), "Create the working directory."
    temp_dir = tempfile.mkdtemp(prefix="export-cafu-src-")
    os.chdir(temp_dir)
    print Info("ok:"), temp_dir


    # Clone the Cafu repository at the latest (HEAD) revision.
    print "\n" + zeit(), "Clone the Cafu repository at the latest (HEAD) revision:", "git clone", git_repo_url, "Cafu"
    subprocess.Popen(["git", "clone", git_repo_url, "Cafu"]).wait()
    print Info("ok")


    # Learn the commit ID, commit date, and construct cafu_src_base_name appropriately.
    print "\n" + zeit(), "Learn the version string."
    os.chdir("Cafu")
    version_string = subprocess.Popen(["git", "show", "-s", "--date=short", "--format=%cd-%h"], stdout=subprocess.PIPE).communicate()[0]
    version_string = version_string.strip()     # Strip the trailing newline character.
    os.chdir("..")

    cafu_src_base_name = "Cafu-bin-" + version_string + "-" + sys.platform
    if sys.platform != "win32":
        cafu_src_base_name += "-" + platform.machine()
    print Info("ok:"), '"' + version_string + '"'


    # Rename directory "Cafu" to cafu_src_base_name.
    print "\n" + zeit(), "Rename directory Cafu to", cafu_src_base_name
    os.rename("Cafu", cafu_src_base_name)
    print Info("ok")


    # Copy the FBX SDK.
    print "\n" + zeit(), "Copy the FBX SDK."
    os.chdir(cafu_src_base_name)

    if fbx_abspath:
        os.remove("ExtLibs/fbx/ReadmeCFS.txt")
        os.rmdir("ExtLibs/fbx")
        shutil.copytree(fbx_abspath, "ExtLibs/fbx")
        print Info("ok")
    else:
        print Warn("skipped")


    # Compile the source code (release build only).
    print "\n" + zeit(), "Compile the source code (release build only)."

    with open(os.devnull, 'w') as fnull:    # --quiet alleine reicht nicht, weil Scons die wxWidgets Makefiles aufruft.
        if sys.platform.startswith("linux"):
            subprocess.Popen(["scons", "-Q", "--quiet", "bv=r", "-j", str(multiprocessing.cpu_count())], stdout=fnull).wait()
        else:
            with open('CompilerSetup.py.tmpl', 'r') as f1:
                with open('CompilerSetup.py', 'w') as f2:
                    for line in f1:
                        line = line.replace('envCommon = Environment()', 'envCommon = Environment(TARGET_ARCH="x86")')
                        f2.write(line)

            subprocess.Popen(["scons.bat", "-Q", "--quiet", "bv=r"], stdout=fnull).wait()

    print Info("ok")


    # Add the contents of the Textures.zip and World.zip files.
    print "\n" + zeit(), "Add the contents of the Textures.zip and World.zip files."

    z = zipfile.ZipFile(Textures_zip, 'r')
    z.extractall("Games/DeathMatch/Textures")

    z = zipfile.ZipFile(Worlds_zip, 'r')
    z.extractall("Games/DeathMatch/Worlds")

    print Info("ok")


    # Move all core binaries into the top-level directory.
    print "\n" + zeit(), "Move all core binaries into the top-level directory."

    SCONS_BUILD_DIR = FindSConsBuildDir()
    ExeSuffix = ".exe*" if sys.platform == "win32" else ""      # Under Windows, copy .exe and .exe.manifest files.
    DllPattern = "*.dll" if sys.platform == "win32" else "lib*.so"

    Programs = [
        "Ca3DE/Cafu",
        # "Ca3DE/Cafu-dedicated",   # Not built at this time.
        "CaBSP/CaBSP",
        "CaPVS/CaPVS",
        "CaLight/CaLight",
        "CaSHL/CaSHL",
        # "CaWE/CaWE",      # See below.
        # "CaSanity",       # Not for the demo...
        "MakeFont",
        "MaterialViewer",
        # "ModelViewer"     # This one is not even built any more.
    ]

    # Move the Cafu engine, all compilers and all tools.
    # They are moved from bin/ to . after their name-clashing directories have been removed.
    os.mkdir("bin")
    for p in Programs:
        myMove(SCONS_BUILD_DIR + "/" + p + ExeSuffix, "./bin/")

    # Under Linux, move CaWE to CaWE-bin, in order to not name-clash with the directory named "CaWE"!
    # Note that "CaWE-bin" is referred to in the shell-script below.
    myMove(SCONS_BUILD_DIR + "/CaWE/CaWE" + ExeSuffix, "./CaWE-bin" if not ExeSuffix else "./bin/")

    # Move all the renderer and sound DLLs.
    for d in ["MaterialSystem", "SoundSystem"]:
        myMove("Libs/" + SCONS_BUILD_DIR + "/" + d + "/" + DllPattern, ".")


    # Add the "run" shell scripts. Their essential task is to set the cwd correctly before program start.
    # This seems to be especially important under KDE/kubuntu, while under Gnome/ubuntu, clicking directly
    # on the binaries starts with the cwd correctly set.
    if sys.platform.startswith("linux"):
        print "\n" + zeit(), "Add the \"run\" shell scripts."

        with open("Cafu-run", 'w') as f:
            f.write("#!/bin/bash\n")
            f.write("cd `dirname \$0`\n")
            f.write("./Cafu\n")

        with open("CaWE-run", 'w') as f:
            f.write("#!/bin/bash\n")
            f.write("cd `dirname \$0`\n")
            f.write("./CaWE-bin\n")

        os.chmod("Cafu-run", 0755)      # Shell scripts require the executable flag to be set.
        os.chmod("CaWE-run", 0755)      # Shell scripts require the executable flag to be set.
        print Info("ok")


    # Delete all source and most build directories.
    print "\n" + zeit(), "Delete all source and most build directories."

    source_dirs = [
        ".git*",
        "build",
        "Ca3DE",
        "CaBSP",
        "CaLight",
        "CaPVS",
        "CaSHL",
        "CaTools",
        "CaWE",
        "Common",
        "Doxygen",
        "ExtLibs",
        # "Fonts",
        # "Games",
        "Libs",
        # "Misc",
        "Games/*.hpp",
        "Games/DeathMatch/Code",
        "Games/DeathMatch/Textures/Q1",
        "Games/DeathMatch/Textures/SkyDomes",
        "Games/DeathMatch/Worlds/AEonsCube*",
        "Games/DeathMatch/Worlds/Gotham*",
        "Games/DeathMatch/Worlds/JrBaseHQ*",
        "Games/DeathMatch/Worlds/Test*",
        "Games/VSWM",
        ".scon*",
        "CompilerSetup*",
        "SCons*",
        "premake*"
    ]

    if sys.platform.startswith("linux"):
        source_dirs += ["config.log"]

    myMove(source_dirs, "..")
    myMove("bin/*", ".")
    myMove("bin", "..")

    # Undo a few of the moves of files that we really want to keep.
    myMove("../CaWE/res", "CaWE/res")

    # We switched from dynamic to static linking for the game libraries in July 2012
    # (r572 and r574), so that there are no longer any DLLs to be moved here.
    # os.makedirs("Games/DeathMatch/Code/" + SCONS_BUILD_DIR + "/")
    # myMove("../Code/" + SCONS_BUILD_DIR + "/" + DllPattern, "Games/DeathMatch/Code/" + SCONS_BUILD_DIR + "/")

    os.makedirs("Games/DeathMatch/Textures/Q1")
    myMove("../Q1/sliptopsd_*.png", "Games/DeathMatch/Textures/Q1")

    os.makedirs("Games/DeathMatch/Textures/SkyDomes")
    myMove("../SkyDomes/Thrawn*", "Games/DeathMatch/Textures/SkyDomes")


    # Create a compressed archive.
    print "\n" + zeit(), "Create a compressed archive."
    os.chdir("..")

    # tar (but not zip) preserves file permissions, i.e. the executable bit of our binaries.
    try:
        fn = os.path.basename(shutil.make_archive(cafu_src_base_name, "zip" if sys.platform == "win32" else "gztar", ".", cafu_src_base_name))
    except AttributeError:
        # Python 2.6 does not yet have the make_archive function, so fall back to "tar" on the shell:
        fn = cafu_src_base_name + ".tar.gz"
        subprocess.Popen(["tar", "czf", fn, cafu_src_base_name]).wait()

    print Info("ok:"), fn + ",", os.path.getsize(fn), "bytes"


    # Upload the files.
    print "\n" + zeit(), "Upload", fn

    ssh.connect('ftp.cafu.de', username=ftp_username, password=ftp_password)
    sftp = ssh.open_sftp()

    if fn in sftp.listdir('cafu/www/files'):
        print Warn("not uploaded:"), "File already at webserver."
    else:
        sftp.put(fn, 'cafu/www/files/' + fn + ".part")
        sftp.rename('cafu/www/files/' + fn + ".part", 'cafu/www/files/' + fn)
        print Info("ok")

    sftp.close()


    # Clean up - remove the temporary directory with the exported files.
    print "\n" + zeit(), "Clean up - remove the temporary working directory", temp_dir

    def OnRmTreeError(func, path, exc_info):
        print Warn("Warning:"), "{0}({1}) failed: {2}".format(func.__name__, path, exc_info)

    # chdir out of the tree we're about to remove.
    # This is required under Windows, where without this, the final rmdir of the topmost
    # directory fails with Windows error 32 ("resource is used by another process").
    os.chdir("..")

    shutil.rmtree(temp_dir, onerror=OnRmTreeError)
    print Info("ok")


if __name__ == "__main__":
    try:
        Main()
        print Info("\nAll done. End of script.")
        if 'shutdown' in args and args.shutdown:
            subprocess.Popen(["shutdown", "/s", "/f", "/t", "300"]).wait()
    except Exception as e:
        print Warn("Error: {0}".format(e))
        traceback.print_exc()
        sys.exit(1)
